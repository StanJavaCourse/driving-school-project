CREATE DATABASE drivingschool;

CREATE TABLE teachers (
	id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    fullname varchar(255) NOT NULL,
    phone varchar(255) NOT NULL,
    address varchar(255),
    email varchar(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
INSERT INTO teachers (fullname, phone, address, email)
VALUES ('Stenly Bin', '57847623', 'Gonsiori 6-11', 'stenly@mail.com');

-- DROP TABLE courses;
CREATE TABLE courses (
	id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    course_name varchar(255) NOT NULL,
    category varchar(255) NOT NULL,
    start_date DATE,
    end_date DATE,
    teacher_id int,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    );
    
ALTER TABLE `drivingschool`.`courses` 
ADD INDEX `fk_course_to_teachers_idx` (`teacher_id` ASC) VISIBLE;
;
ALTER TABLE `drivingschool`.`courses` 
ADD CONSTRAINT `fk_course_to_teachers`
  FOREIGN KEY (`teacher_id`)
  REFERENCES `drivingschool`.`teachers` (`id`)
  ON DELETE RESTRICT
  ON UPDATE NO ACTION;
    
INSERT INTO courses (course_name, category, start_date, end_date, teacher_id)
VALUES ('50-T-A-50-23', 'B-category', CURDATE(), CURDATE(), 1);

-- DROP TABLE students;
CREATE TABLE students (
	id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    fullname varchar(255) NOT NULL,
    id_code varchar(255) NOT NULL,
    phone varchar(255) NOT NULL,
    address varchar(255),
    email varchar(255) NOT NULL,
    course_id int,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

ALTER TABLE `drivingschool`.`students` 
ADD UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
ADD UNIQUE INDEX `id_code_UNIQUE` (`id_code` ASC) VISIBLE,

ADD INDEX `fk_student_to_course_idx` (`course_id` ASC) VISIBLE;
ALTER TABLE `drivingschool`.`students` 
ADD CONSTRAINT `fk_student_to_course`
  FOREIGN KEY (`course_id`)
  REFERENCES `drivingschool`.`courses` (`id`)
  ON DELETE RESTRICT
  ON UPDATE NO ACTION;

INSERT INTO students (fullname, id_code, phone, address, email, course_id)
VALUES ('James Peterson', '38726653521', '567834832', 'Majaka 3-109', 'stan@mail.com', 1);



