**NAME**

Driving School's Management System


**DESCRIPTION:**

This is a practical project for Java beginner course. Main agenda is to showcase learned Java basic skills by creating application, including simple database using MySQL Database Engine.

The main idea of this project is to simplify and collect the data of students who register in a driving school and then link the obtained data with other databases so as not to enter the same data again. Connect all processes together and make data processing fast and convenient.

This program will speed up data processing and will be convenient for student and the driving school management.


**IMPLEMENTED LOGIC:**

Program starts with a welcome massage: “Welcome to Driving School Management System”

At the moment there are available following operations:

p - get all data of registered students from database,
a - add new student to database and choose a course for this student by courseID. 
t -  add new teacher to database,
c - add new course to database and enter the startDate/endDate according to the given pattern (dd.mm.yyyy). If you enter the wrong date format, the program will show an error with massage “Invalid date format!” and offer a new attempt to enter the date with a message “Please try again: (dd.mm.yyyy)”.
d - prints all courses and allows you to select and delete course by ID from the database,
e - exit the program with a massage: “Good bye”.

**ROADMAP**

In final project will be added new methods such as: remove teacher, remove student, create driving card, add teacher to driving card and add student to driving card.


**CODE:**

Code is written in Java on IntellJ platform and database is created in MySQL Database Engine.
